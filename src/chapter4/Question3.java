package chapter4;

import java.util.LinkedList;

public class Question3 {
	public <T> void depthLinkedList(CJBTree<T> tree) {
		LinkedList<CJBNode<T>> ll = new LinkedList<CJBNode<T>>();
		CJBNode<T> root = tree.getRoot();
		ll.add(root);
		CJBNode<T> depthNode = root;
		int depth = 0;
		while(depthNode.getFirstChild() != null) {
			depthNode = depthNode.getFirstChild();
			depth++;
		}
		LinkedList<LinkedList<CJBNode<T>>> llll = new LinkedList<LinkedList<CJBNode<T>>>();
		for(int i = 0; i <= depth ; i++) {
			llll.add(new LinkedList<CJBNode<T>>());
		}
		while(!ll.isEmpty()) {
			CJBNode<T> r = ll.removeFirst();
			for(int i = 0 ; i <= depth ; i++) {
				if(getDepth(r) == i) {
					llll.get(i).add(r);
				}
			}
			for(CJBNode<T> node : r.getChildren()) {
				if(node != null) {
					ll.add(node);
				}
			}
		}
		for(LinkedList<CJBNode<T>> lll : llll) {
			System.out.println("this depth : " + llll.indexOf(lll));
			for(CJBNode<T> t : lll) {
				System.out.println(t.getItem());
			}
		}
	}
	public <T> int getDepth(CJBNode<T> node) {
		if(node.getParent() == null) {
			return 0;
		}
		return getDepth(node.getParent()) + 1;
	}
	public static void main(String[] args) {
		Question3 obj = new Question3();
		CJBTree<Integer> test = new CJBTree<Integer>();
		for(int i = 0; i < 11; i++) {
			test.insertNode(new CJBNode<Integer>(i));
		}
		obj.depthLinkedList(test);
		test.dPrintTree();
	}
}
