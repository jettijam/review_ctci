package chapter4;

import java.util.ArrayList;

public class Question8 {
	
	public JMNode firstCommonAncestor(JMMyTree tree, JMNode n1, JMNode n2){
		ArrayList<JMNode> route1 = rootToNode(tree, n1);
		ArrayList<JMNode> route2 = rootToNode(tree, n2);
		for(int idx1 = route1.size()-1 ; idx1 > -1 ; idx1--){
			String s1 = route1.get(idx1).name;
			for(int idx2 = route2.size()-1 ; idx2 > -1 ; idx2--){
				String s2 = route2.get(idx2).name;
				if(s1.equals(s2))
					return route1.get(idx1);
			}
		}
		return null;
	}
	
	public ArrayList<JMNode> rootToNode(JMMyTree tree, JMNode n){
		ArrayList<JMNode> result = new ArrayList<JMNode>();
		//DFS To find rout from root to the node n
		dfsRootToNode(tree, n, result);
		
		return result;
	}
	
	public boolean dfsRootToNode(JMMyTree tree, JMNode n , ArrayList<JMNode> result){
		if(tree.root == n){
			result.add(tree.root);
			return true;
		}
		if(tree.root.children.isEmpty()){
			return false;
		}
		for(JMNode c : tree.root.children){
			boolean res = dfsRootToNode(new JMMyTree(c), n, result);
			if(res){
				result.add(0, tree.root);
				return res;
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		int arr[] = new int[6];
		arr[0] = 2;
		arr[1] = 4;
		arr[2] = 6;
		arr[3] = 8;
		arr[4] = 10;
		arr[5] = 20;
		Question2 q2 = new Question2();
		JMMyTree tree = q2.minimalTree(arr);
		q2.dfs(tree.root, 0);
		Question8 test = new Question8();
		
//		ArrayList<Node> tmp = test.rootToNode(tree, tree.root.children.get(1).children.get(0));
		System.out.println("$$$$$$$$$");
//		for(Node n : tmp){
//			System.out.println(n.name);
//		}
		
		JMNode result = test.firstCommonAncestor(tree, tree.root.children.get(0).children.get(1), tree.root.children.get(0).children.get(0));
		System.out.println(result.name);
	}

}
