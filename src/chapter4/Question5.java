package chapter4;

public class Question5 {
	public boolean validateBST(JMMyTree tree){
//		Question4 test = new Question4();
//		if(!test.checkBalanced(tree))
//			return false;
//		
		
		
		int cur = Integer.valueOf(tree.root.name);
		JMMyTree left = null;
		JMMyTree right = null;
		
		if(tree.root.children.size() > 0 && tree.root.children.get(0) != null){
			left = new JMMyTree(tree.root.children.get(0));
		}
		if(tree.root.children.size() > 1 && tree.root.children.get(1) != null){
			right = new JMMyTree(tree.root.children.get(1));
		}
		
		int left_max = Integer.MIN_VALUE;
		if(left!=null)
			maxVal(left);
		int right_min = Integer.MAX_VALUE;
		if(right != null)
			minVal(right);
//		System.out.println("left_max : "+left_max);
//		System.out.println("rght_min : "+right_min);
		if (left_max > cur)
			return false;
		if (right_min < cur)
			return false;
		
		boolean result = true;
		if(left != null){
			result &= validateBST(left);
		}
		if(right != null){
			result &= validateBST(right);
		}
		
		return result;
	}
	
	public int maxVal(JMMyTree tree){
		int cur = Integer.valueOf(tree.root.name);
		
		JMMyTree left = null;
		JMMyTree right = null;
		
		if(tree.root.children.size() > 0 && tree.root.children.get(0) != null){
			left = new JMMyTree(tree.root.children.get(0));
		}
		if(tree.root.children.size() > 1 && tree.root.children.get(1) != null){
			right = new JMMyTree(tree.root.children.get(1));
		}
		
		int left_max = 0;
		int right_max = 0;
		if(left != null){
			left_max = maxVal(left);
		}
		if(right != null){
			right_max = maxVal(right);
		}
		
		return Math.max(cur, Math.max(left_max, right_max));
	}
	
	public int minVal(JMMyTree tree){
		int cur = Integer.valueOf(tree.root.name);
		
		JMMyTree left = null;
		JMMyTree right = null;
		
		if(tree.root.children.size() > 0 && tree.root.children.get(0) != null){
			left = new JMMyTree(tree.root.children.get(0));
		}
		if(tree.root.children.size() > 1 && tree.root.children.get(1) != null){
			right = new JMMyTree(tree.root.children.get(1));
		}
		
		int left_min = Integer.MAX_VALUE;
		int right_min = Integer.MAX_VALUE;
		if(left != null){
			left_min = minVal(left);
		}
		if(right != null){
			right_min = minVal(right);
		}
		
		return Math.min(cur, Math.min(left_min, right_min));
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[] = new int[6];
		arr[0] = 2;
		arr[1] = 4;
		arr[2] = 6;
		arr[3] = 8;
		arr[4] = 10;
		arr[5] = 20;
		Question2 q2 = new Question2();
		JMMyTree tree = q2.minimalTree(arr);
		tree.root.children.remove(1);
		
		q2.dfs(tree.root, 0);
		Question5 test = new Question5();
		System.out.println(test.validateBST(tree));
	}

}
