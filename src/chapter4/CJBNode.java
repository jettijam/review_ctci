package chapter4;

import java.util.LinkedList;

public class CJBNode<T> {
	private T item;
	private LinkedList<CJBNode<T>> children = new LinkedList<CJBNode<T>>();
	private LinkedList<CJBNode<T>> adjacentNodes = new LinkedList<CJBNode<T>>();
	private CJBNode<T> parent = null;
	private CJBNode<T> nextSibling = null;
	private int depth = 0;
	public void addChild(CJBNode<T> n) {
		if(n != null) {
			if(this.children.size() > 0) {
				this.getLastChild().nextSibling = n;
			}
			this.children.add(n);
			n.setParent(this);
			n.depth = this.depth + 1;
		}
	}
	public void setParent(CJBNode<T> n) {
		this.parent = n;
	}
	public CJBNode<T> getFirstChild() {
		if(this.children.size() == 0) {
			return null;
		}
		return this.children.getFirst();
	}
	public CJBNode<T> getLastChild() {
		if(this.children.size() == 0) {
			return null;
		}
		return this.children.getLast();
	}
	public CJBNode<T> getParent() {
		return this.parent;
	}
	public void removeLastChild() {
		this.children.getLast().setParent(null);
		this.children.removeLast();
	}
	public void removeFirstChild() {
		this.children.getFirst().setParent(null);
		this.children.removeFirst();
	}
	public LinkedList<CJBNode<T>> getChildren() {
		return this.children;
	}
	public void setChildren(LinkedList<CJBNode<T>> children) {
		this.children = children;
	}
	public CJBNode(T item) {
		this.item = item;
	}
	public T getItem() {
		return this.item;
	}
	public CJBNode<T> getNextSibling() {
		return this.nextSibling;
	}
	public int getDepth() {
		return this.depth;
	}
	public void setAdjacentNode(CJBNode<T> node) {
		this.adjacentNodes.add(node);
	}
	public LinkedList<CJBNode<T>> getAdjacentNodes() {
		return this.adjacentNodes;
	}
}
