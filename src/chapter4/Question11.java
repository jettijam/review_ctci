package chapter4;

import java.util.ArrayList;
import java.util.Random;

public class Question11 {
	public JMNode randomNode(JMMyTree tree){
		ArrayList<JMNode> nodeList = new ArrayList<JMNode>();
		dfsNodeList(tree.root, nodeList);
	
		Random r = new Random();
		return nodeList.get(r.nextInt(nodeList.size()));
	}
	
	public void dfsNodeList(JMNode node, ArrayList<JMNode> list){
		list.add(node);
		for(JMNode c : node.children){
			dfsNodeList(c, list);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JMMyTree t1 = new JMMyTree(new JMNode("2"));
		t1.root.children.add(new JMNode("1"));
		t1.root.children.add(new JMNode("5"));
		t1.root.children.get(1).children.add(new JMNode("4"));
		t1.root.children.get(1).children.add(new JMNode("6"));
		
		Question11 test = new Question11();
		System.out.println(test.randomNode(t1).name);

	}

}
