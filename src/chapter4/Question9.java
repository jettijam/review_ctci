package chapter4;

import java.util.ArrayList;
import java.util.LinkedList;

public class Question9 {
	public ArrayList<LinkedList<Integer>> allSequences(CJBNode<Integer> node) {
		ArrayList<LinkedList<Integer>> result = new ArrayList<LinkedList<Integer>>();
		
		if(node == null) {
			result.add(new LinkedList<Integer>());
			return result;
		}
		
		LinkedList<Integer> prefix = new LinkedList<Integer>();
		prefix.add(node.getItem());
		
		ArrayList<LinkedList<Integer>> leftSeq = allSequences(node.getFirstChild());
		ArrayList<LinkedList<Integer>> rightSeq = allSequences(node.getLastChild());
		
		for (LinkedList<Integer> left : leftSeq) {
			for (LinkedList<Integer> right : rightSeq) {
				ArrayList<LinkedList<Integer>> weaved = new ArrayList<LinkedList<Integer>>();
				weaveLists(left, right, weaved, prefix);
				result.addAll(weaved);
			}
		}
		return result;
	}
	public void weaveLists(LinkedList<Integer> first, LinkedList<Integer> second,
			ArrayList<LinkedList<Integer>> results, LinkedList<Integer> prefix) {
		if (first.size() == 0 || second.size() == 0) {
			LinkedList<Integer> result = (LinkedList<Integer>) prefix.clone();
			result.addAll(first);
			result.addAll(second);
			results.add(result);
			return;
		}
		
		int headFirst = first.removeFirst();
		prefix.addLast(headFirst);
		weaveLists(first, second, results, prefix);
		prefix.removeLast();
		first.addFirst(headFirst);
		
		int headSecond = second.removeFirst();
		prefix.addLast(headSecond);
		weaveLists(first, second, results, prefix);
		prefix.removeLast();
		second.addFirst(headSecond);
	}
	
	public static void main(String[] args) {
		Question9 obj = new Question9();
		LinkedList<CJBNode<Integer>> testLL = new LinkedList<CJBNode<Integer>>();
		CJBTree<Integer> test = new CJBTree<Integer>();
		for(int i = 0; i < 10; i++) {
			CJBNode<Integer> node = new CJBNode<Integer>(i);
			testLL.add(node);
			test.insertNode(node);
		}
		test.dPrintTree();
		ArrayList<LinkedList<Integer>> result = obj.allSequences(test.getRoot());
		System.out.println("asdf" + result.get(2));
	}
}
