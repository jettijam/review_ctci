package chapter4;

import java.util.LinkedList;

public class Question6 {
	public CJBNode<Integer> findNextNode(CJBNode<Integer> node) {
		if(node.getChildren().size() == 2) {
			return minNodeFromLeftSubtree(node.getLastChild());
		}
		CJBNode<Integer> searchNode = node;
		while(searchNode != null) {
			if(searchNode.getParent().getFirstChild() == searchNode) {
				return searchNode.getParent();
			}
			searchNode = searchNode.getParent();
		}
		return null;
	}
	public CJBNode<Integer> minNodeFromLeftSubtree(CJBNode<Integer> node) {
		CJBNode<Integer> min = node;
		while(min.getFirstChild() != null) {
			min = min.getFirstChild();
		}
		return min;
	}
	public static void main(String[] args) {
		Question6 obj = new Question6();
		CJBTree<Integer> tree = new CJBTree<Integer>();
		LinkedList<CJBNode<Integer>> ll = new LinkedList<CJBNode<Integer>>();
		for(int i = 0; i < 20; i++) {
			ll.add(new CJBNode<Integer>(i));
		}
		tree.insertNode(ll.get(10));
		tree.insertNode(ll.get(5));
		tree.insertNode(ll.get(15));
		tree.insertNode(ll.get(3));
		tree.insertNode(ll.get(6));
		tree.insertNode(ll.get(13));
		tree.insertNode(ll.get(16));
		tree.dPrintTree();
		System.out.println(obj.findNextNode(ll.get(15)).getItem());
	}
}
