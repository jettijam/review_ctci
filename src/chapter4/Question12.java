package chapter4;

import java.util.HashMap;
import java.util.LinkedList;

public class Question12 {
	public int pathsWithSum(CJBNode<Integer> root, int targetSum) {
		return countPathsWithSum(root, targetSum, 0, new HashMap<Integer, Integer>());
	}
	public int countPathsWithSum(CJBNode<Integer> node, int targetSum, int runningSum, HashMap<Integer, Integer> pathCount) {
		if(node == null) {
			return 0;
		}
		runningSum += node.getItem();
		int diff = runningSum - targetSum;
		int totalPaths = pathCount.getOrDefault(diff, 0);
		
		if(runningSum == targetSum) {
			totalPaths++;
		}
		
		increaseCount(pathCount,  runningSum,  1);
		totalPaths += countPathsWithSum(node.getFirstChild(), targetSum ,  runningSum,  pathCount);
		totalPaths += countPathsWithSum(node.getLastChild(), targetSum, runningSum, pathCount);
		increaseCount(pathCount, runningSum, -1);
		
		return totalPaths;
	}
	public void increaseCount(HashMap<Integer, Integer> pathCount, int runningSum, int diff) {
		int currentCount = pathCount.getOrDefault(runningSum,  0) + diff;
		if(currentCount == 0) {
			pathCount.remove(runningSum);
		}
		else {
			pathCount.put(runningSum,  currentCount);
		}
	}
	public static void main(String[] args) {
		Question12 obj = new Question12();
		LinkedList<CJBNode<Integer>> testLL = new LinkedList<CJBNode<Integer>>();
		CJBTree<Integer> test = new CJBTree<Integer>();
		for(int i = 0; i < 30; i++) {
			CJBNode<Integer> node = new CJBNode<Integer>(i);
			testLL.add(node);
			test.insertNode(node);
		}
		test.dPrintTree();
		System.out.println(obj.pathsWithSum(test.getRoot(), 26));
	}
}
