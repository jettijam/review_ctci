package chapter4;

import java.util.Random;

public class JMGraphBuilder {
	public static JMGraph randomGraph(int num){
		JMGraph result = new JMGraph();
		Random random = new Random();
		result.nodes = new JMNode[num];
		for(int i = 0 ; i < num ; i++){
			result.nodes[i] = new JMNode(String.valueOf(i));
		}
		
		
		for(int i = 0 ; i < (int)num*1 ; i++){
			int src = random.nextInt(num);
			int dst = random.nextInt(num);
			while(src==dst || result.nodes[src].isChild(result.nodes[dst])){
				dst = random.nextInt(num);
			}
			result.nodes[src].children.add(result.nodes[dst]);
			result.nodes[dst].children.add(result.nodes[src]);
		}
		
		return result;
	}
}
