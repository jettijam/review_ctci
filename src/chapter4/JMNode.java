package chapter4;

import java.util.ArrayList;

public class JMNode {
	public String name;
	public ArrayList<JMNode> children = new ArrayList<JMNode>();
	public JMNode(){}
	public JMNode(String name){this.name=name;}
	public boolean isChild(JMNode node){
		return children.contains(node);
	}
	
	public void addChild(JMNode node){
		this.children.add(node);
//		node.children.add(this);
	}
}
