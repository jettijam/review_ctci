package chapter8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Question3_JM {
	int[] magicIndex(int arr[]){
		ArrayList<Integer> list = new ArrayList<Integer>();
		recurMagicIndex(0, 0, arr, list);
		
		return list.stream().mapToInt(i -> i).toArray();
	}
	
	boolean recurMagicIndex(int pos, int idx, int arr[], ArrayList<Integer> list){
		if(arr[pos]!=idx){
			if(idx == 0){
				if(pos+1 < arr.length){
					return recurMagicIndex(pos+1, idx, arr, list);
				}
			}
			return false;
		}
		list.add(arr[pos]);
		if(pos+1 < arr.length){
			recurMagicIndex(pos+1, idx+1, arr, list);
		}
		return true;
	}
	
	public static void main(String[] args){
		Random r = new Random();
		int arr[] = new int[30];
		for(int i = 0 ; i < arr.length ; i++){
			arr[i] = r.nextInt(50)-25;
		}
		Arrays.sort(arr);
		System.out.println("INPUT ARR");
		for(int i : arr){
			System.out.print(i+" ");
		}
		System.out.println();
		
		Question3_JM test = new Question3_JM();
		int magic[] = test.magicIndex(arr);
		System.out.println("MAGIC ARR");

		for(int i : magic){
			System.out.print(i+" ");
		}
		System.out.println();	}
}
