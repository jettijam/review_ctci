package chapter8;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class Question14_JM {	
	Set<String> set_deprecated = new HashSet<String>();
	
	public int atomEval(String input){
		if(input.length() == 1){
			return input.charAt(0) == '0'?0:1;
		}
		int a = input.charAt(0) == '0'?0:1;
		int b = input.charAt(2) == '0'?0:1;
		
		switch(input.charAt(1)){
		case '&':
			return a&b;
		case '|':
			return a|b;
		case '^':
			return a^b;
			
		}
		
		return 1;
	}
	
	public int plainEval(String input){
		//괄호 ?��?�� 경우�? eval
		while(input.length() > 3){
			String subs = input.substring(0, 3);
			input = String.valueOf(atomEval(subs))+input.substring(3);
		}
		
		return atomEval(input);
	}
	
	public int eval(String input){
//		System.out.println("eval input : "+input);
		
		
		
		// 괄호�? ?��?��??�? plainEval�? 바꿔
		while(input.contains("(")){
			if(!input.contains(")"))
				break;
			String sub = "";
			int leftIdx = -1;
			for(int i = 0 ; i < input.length() ; i++){
//				System.out.println(input+" "+leftIdx + " "+i+" "+sub);
				
				if(input.charAt(i) == '('){
					sub = "";
					leftIdx = i;
				}else if(input.charAt(i) == ')'){
					input = input.substring(0, leftIdx)
							+ plainEval(sub)
							+ input.substring(i+1);
					break;
				}else{
					sub = sub + input.charAt(i);
				}
				
			}
	
		}
		
		
		return plainEval(input);
	}
	
	/***
	 * 
	 * @param input
	 * @param posArr
	 * @return append left/right parenthesis inside input (w/o validation)
	 */
	public String posArrToParen(String input, int posArr[]){
		
		String result = "";
		for(int i = 0 ; i < input.length() ; i++){
			if(posArr[i] > 0 ){
				for(int j = 0 ; j < posArr[i] ; j++){
					result = result + '(';
				}
			}else if(posArr[i] < 0){
				for(int j = 0 ; j > posArr[i] ; j--){
					result = result + ')';
				}
			}
			result = result + input.charAt(i);
		}
		if(posArr[input.length()] > 0){
			for(int j = 0 ; j < posArr[input.length()] ; j++){
				result = result + '(';
			}
		}else if(posArr[input.length()] < 0){
			
			for(int j = 0 ; j > posArr[input.length()] ; j--){
				result = result + ')';
			}		
		}
		
		return result;

	}
	
	public int[] numRangeSetToPosArr(String input, Set<NumRange> set){
		int posArr[] = new int[input.length() + 1];
		Iterator<NumRange> itr = set.iterator();
		while(itr.hasNext()){
			NumRange n = itr.next();
			if(posArr[n.start] < 0){
				System.out.println("Duplicate location!");
				System.exit(0);
			}
			if(posArr[n.end] > 0){
				System.out.println("Duplicate location!");
				System.exit(0);
			}
			posArr[n.start] += 1;
			posArr[n.end] += -1;
		}
		
		return posArr;
	}
	
	
	public boolean validateInput(String input){ // Validate parenthesis
		Stack<Integer> lStack = new Stack<Integer>();
		
		for(int i = 0 ; i < input.length() ; i++){
			if(input.charAt(i) == '('){
				lStack.push(i);
			}else if(input.charAt(i) == ')'){
				if(lStack.isEmpty())
					return false;
				int l = lStack.pop();
				if(i == l+2){
					return false;
				}
				
				
			}
			
		}
		
		
		return lStack.empty();
	}
	
	public int recurBooleanEvaluation(String input, boolean result, Set<NumRange> localSet, int[] posArr){
//		int posArr[] = numRangeSetToPosArr(input, localSet);
		String createdInput = posArrToParen(input, posArr);
		
//		System.out.println("Now Evaluating : " + createdInput);
		int ret = 0;
		
		if(!set_deprecated.contains(createdInput)){
			boolean evalResult = (eval(createdInput) == 1);
			if(evalResult == result){
				System.out.println("!!!!! wow :"+createdInput);
				ret++;
			}
			
			set_deprecated.add(createdInput);
		}
		int left = -1;
		int right = -1;
		
		for(int i = 0 ; i < posArr.length ; i+=2){ // to create new left parenthesis
			if(posArr[i] < 0){ //When right paren exists, continue
				System.out.println("Oops right exists :"+i);
				continue;
			}
			
			int newPosArr[] = new int[posArr.length];
			for(int t = 0 ; t < posArr.length ; t++){
				newPosArr[t] = posArr[t];
			}
			
			Set<NumRange> newLocalSet = new HashSet<NumRange>();
			Iterator<NumRange> itr = localSet.iterator();
			while(itr.hasNext()){
				newLocalSet.add(itr.next());
			}
			
			newPosArr[i] += 1;
			left = i;
			
			for(int j = i+1 ; j < newPosArr.length ; j+=2){ // for right
//				System.out.println("###### for left("+i+"), choose right ("+j+")");
				if(newPosArr[j] > 0){ //when left paren exists, continue
//					System.out.println("Oops left exists :"+j);
					continue;
				}
				
				right = j;
				
				NumRange range = new NumRange(left, right);
				if(localSet.contains(range)){
//					System.out.println("Contains!");
					continue;
				}
				
				boolean intersects = false;
				Iterator<NumRange> itr2 = newLocalSet.iterator();
				while(itr2.hasNext()){
					NumRange n = itr2.next();
					if(n.intersect(range)){
//						System.out.println("Intersects!");
						intersects = true;
						break;
					}
				}
				if(intersects){
					continue;
				}
				
				newPosArr[j] += -1;
				
				String created = posArrToParen(input, newPosArr);
//				
				if(set_deprecated.contains(created)){
//					System.out.println("String duplicated");
					newPosArr[j] ++;
					continue;
				}
				if(validateInput(created)){
					
					newLocalSet.add(range);
//					set_deprecated.add(created);
//					System.out.println("Let's go with "+created);
					ret += recurBooleanEvaluation(input, result, newLocalSet, newPosArr);
				}else{
//					System.out.println("Not valid!" + created);
				}
				newPosArr[j] ++;
			}
			
		}
		
//		System.out.println("Result  " + posArrToParen(input, posArr) + "  >> "+ret);

		return ret;	
	}
	
	public int booleanEvaluation(String input, boolean result){
		Set<NumRange> localSet = new HashSet<NumRange>();
		int posArr[] = new int[input.length()+1];
//		localSet.add(new NumRange(0, input.length()));
		int ret = recurBooleanEvaluation(input, result, localSet, posArr);
		return ret;
	}
	
	public static void main(String[] args){
		Question14_JM test = new Question14_JM();
//		System.out.println(test.eval("(1^(0)|0)|1"));
		
//		String input = "0&0&0&1^1|0";
		String input = "1^0|0|1";
//		int[] posArr = new int[input.length()+1];
//		posArr[0] = 1;
//		posArr[2] = -1;
//		posArr[4] = 1;
//		posArr[5] = -1;
//		
//		System.out.println(test.posArrToParen("1^0|0|1", posArr));
		System.out.println(test.booleanEvaluation(input, false));
		
		System.out.println("I looked for ==========");
		Iterator<String> itr = test.set_deprecated.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
	}
}
