package chapter8;

import java.util.HashSet;
import java.util.Set;

public class NumRange {
	public int start = -1;
	public int end = -1;
	
	public NumRange(int start, int end){
		this.start = start;
		this.end = end;
	}
	
	public int length(){
		return end-start;
	}
	
	public boolean intersect(NumRange other){
		NumRange left = this.start < other.start? this : other;
		if(this.start == other.start){
			left = this.length() >= other.length()? this : other;
					
		}
		NumRange right = this.end > other.end? this : other;
		if(this.start == other.start){
			right = this.length() >= other.length()? this : other;
					
		}
		
		if(left == right){
			return false;
		}
		
		if(left.end <= right.start){
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode(){
		return this.start*10 + this.end;
	}
	
	@Override
	public boolean equals(Object other){
		if(!other.getClass().equals(this.getClass())){
			return false;
		}
		NumRange o = (NumRange)other;
		
		return (o.hashCode() == this.hashCode());
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NumRange a = new NumRange(0, 7);
		NumRange b = new NumRange(0, 7);
		
		System.out.println(b.intersect(a));
		
		Set<NumRange> set = new HashSet<NumRange>();
		
		set.add(a);
		System.out.println(set.contains(b));
	}

}
