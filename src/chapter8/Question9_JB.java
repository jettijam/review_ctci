package chapter8;

import java.util.ArrayList;

public class Question9_JB {
	public void printPossibleCombinations(int count) {
		ArrayList<String> list = new ArrayList<String>();
		list = printPossibleCombinations(count, list);
		System.out.println(list);
	}
	
	public ArrayList<String> printPossibleCombinations(int count, ArrayList<String> list) {
		if(count == 1) {
			list.add("()");
			return list;
		}
		else {
			list = printPossibleCombinations(count-1, list);
			ArrayList<String> tempList = new ArrayList<String>();
			// 1개의 괄호 pair 를 추가하는 과정은
			// 앞뒤에 추가하기, 두개를 붙여서 앞에 추가하기, 뒤에 추가하기 의 3가지 방법
			for(String s : list) {
				String type1 = "(" + s + ")";
				String type2 = "()" + s;
				String type3 = s + "()";
				if(tempList.contains(type1) == false) {
					tempList.add(type1);
				}
				if(tempList.contains(type2) == false) {
					tempList.add(type2);
				}
				if(tempList.contains(type3) == false) {
					tempList.add(type3);
				}
			}
			return tempList;
		}
	}
	
	public static void main(String[] args) {
		Question9_JB obj = new Question9_JB();
		obj.printPossibleCombinations(3);
	}
}
