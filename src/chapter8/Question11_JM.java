package chapter8;

import java.util.HashSet;
import java.util.Set;

public class Question11_JM {
	
	int dynamic[];
	int coins[] = {25, 10, 5, 1};
	
	public int coins(int n) {
		
		return recurCoins_old(n, 0, 0, 0);
		
	}
	
	public int dynamicCoins(int n){
		dynamic = new int[n+1];
		dynamic[0] = 1;

		for(int i = 0 ; i < 4 ; i++) {
			for(int j = coins[i] ; j <= n ; j++) {
				dynamic[j] += dynamic[j - coins[i]];
			}
		}
		return dynamic[n];
	}
	
	public int recurCoins_old(int n, int cur, int coin, int result) {
		cur += coin;
		if(cur == n) {
			result ++;
			return result;
		}
		if(cur > n) {
			return result;
		}
		result = recurCoins_old(n, cur, 25, result) + recurCoins_old(n, cur, 10, result) + recurCoins_old(n, cur, 5, result) + recurCoins_old(n, cur, 1, result);
		return result;
	}
	
	public int recurCoins(int n, int[] numCoins) {
		return 0;
	}
	
	public static void main(String[] args) {
		Question11_JM test = new Question11_JM();
		System.out.println(test.dynamicCoins(6));
	}

}
