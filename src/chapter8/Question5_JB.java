package chapter8;

public class Question5_JB {
	public int recMult(int a, int b) {
		// 더 좋은 방법이 있을 것....
		if(a < b) {
			return recMult(b, a);
		}
		if(b == 1) {
			return a;
		}
		else {
			return recMult(a, b-1) + a;
		}
	}
	
	public static void main(String[] args) {
		Question5_JB obj = new Question5_JB();
		System.out.println(obj.recMult(5, 100));
	}
}
