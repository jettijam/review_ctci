package chapter8;

import java.util.ArrayList;

public class Question7_JM {
	public ArrayList<String> permutation(String str){
		ArrayList<String> result = new ArrayList<String>();
		recurPerm("", str, result);
		return result;
	}
	
	public void recurPerm(String pre, String rem, ArrayList<String> result) {
		if(rem.equals("")) {
			result.add(pre);
			return;
		}
		for(int i = 0 ; i < rem.length() ; i++) {
			recurPerm(pre + rem.charAt(i), rem.substring(0, i)+rem.substring(i+1, rem.length()), result);
			
		}
		return;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question7_JM test = new Question7_JM();
		ArrayList<String> result = test.permutation("ABC");
		for(String s : result) {
			System.out.println(s);
		}
	}

}
