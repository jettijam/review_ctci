package chapter8;

import java.util.Arrays;

public class Question1_JB {
	public int countWaysForStep(int stair)
	{
		// 속도 증가를 위한 임시저장용 array
		int[] memo = new int[stair+1];
		// 답이 나오지 않은 index 를 -1 로 초기화
		Arrays.fill(memo,  -1);
		return countWaysForStep(stair, memo);
	}
	
	public int countWaysForStep(int stair, int[] memo) {
		if(stair < 0) {
			return 0;
		}
		if((stair == 1) || (stair == 0)) {
			return 1;
		}
		if(memo[stair] == -1) {
			// 한 번에 3칸까지 이동 가능하므로 3칸, 2칸, 1칸 이전까지의 이동 가능 경우의 수를 더함
			memo[stair] = countWaysForStep(stair-3, memo) + countWaysForStep(stair-2, memo) + countWaysForStep(stair-1, memo);
		}
		return memo[stair];
	}
	public static void main(String[] args) {
		Question1_JB obj = new Question1_JB();
		System.out.println(obj.countWaysForStep(4));
	}
}
