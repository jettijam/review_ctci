package chapter8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Question13_JB {
	class Box {
		private int height = -1;
		private int depth = -1;
		private int width = -1;
		
		Box(int h, int d, int w) {
			this.height = h;
			this.depth = d;
			this.width = w;
		}
		
		int getWidth() {
			return this.width;
		}
		
		int getDepth() {
			return this.depth;
		}
		
		int getHeight() {
			return this.height;
		}
		
		boolean canBeAbove(Box x) {
			return ((this.width < x.getWidth()) && (this.depth < x.getDepth()));
		}
	}
	
	class BoxComparator implements Comparator<Box> {
		@Override
		public int compare(Box x, Box y) {
			return y.getHeight()-x.getHeight();
		}
	}
	
	int createStack(ArrayList<Box> boxes) {
		Collections.sort(boxes, new BoxComparator());
		int maxHeight = 0;
		int[] stackMap = new int[boxes.size()];
		for (int i = 0; i < boxes.size(); i++) {
			int height = createStack(boxes, i, stackMap);
			maxHeight = Math.max(maxHeight, height);
		}
		return maxHeight;
	}
	
	int createStack(ArrayList<Box> boxes, int boxIndex, int[] stackMap) {
		if (boxIndex < boxes.size() && stackMap[boxIndex] > 0) {
			return stackMap[boxIndex];
		}
		
		Box bottom = boxes.get(boxIndex);
		int maxHeight = 0;
		for (int i = boxIndex + 1; i < boxes.size(); i++) {
			if (boxes.get(i).canBeAbove(bottom)) {
				int height = createStack(boxes, i, stackMap);
				maxHeight = Math.max(height, maxHeight);
			}
		}
		maxHeight += bottom.height;
		stackMap[boxIndex] = maxHeight;
		return maxHeight;
	}
	
	public static void main(String[] args) {
		Question13_JB obj = new Question13_JB();
		ArrayList<Box> boxes = new ArrayList<Box>();
		boxes.add(obj.new Box(3,4,3));
		boxes.add(obj.new Box(4,5,5));
		boxes.add(obj.new Box(4,7,6));
		System.out.println(obj.createStack(boxes));
		for(int i = 0 ; i < boxes.size(); i++) {
			System.out.println("("+boxes.get(i).getHeight()+","+boxes.get(i).getDepth()
					+","+boxes.get(i).getWidth()+")");
		}
	}
}
