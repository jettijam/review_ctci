package chapter2;

public class Question2 {
	void returnKthToLast(CJBLinkedList ll, int index) {
		CJBLinkedList.Node n = ll.head;
		for(int i = 0; i < index -1; i++) {
			n = n.next;
		}
		ll.head = n;
	}
	public static void main(String[] args) {
		Question2 obj = new Question2();
	    CJBLinkedList test = new CJBLinkedList();
	    for(int i = 0; i < 10; i++) {
	    	test.appendToTail(i);
	    }
	    test.printList();
	    System.out.println(" ");
	    System.out.println("===");
	    obj.returnKthToLast(test, 5);
	    test.printList();
	}
}
