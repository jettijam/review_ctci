package chapter2;

public class Question3 {
	public void deleteMiddleNode(LinkedListNode list, int val){
		LinkedListNode.Node p = list.head;
		while(p!=null){
			if(p.next == null)
				break;
			if(p.next.data == val){
				p.next = p.next.next;
				break;
			}
			p = p.next;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedListNode list = new LinkedListNode();
		list.appendToTail(1);
		list.appendToTail(2);
		list.appendToTail(3);
		list.appendToTail(4);
		list.appendToTail(5);
		list.appendToTail(6);
		list.appendToTail(7);
		
		list.printList();
		System.out.println();
		
		Question3 test = new Question3();
		test.deleteMiddleNode(list,  4);
		list.printList();
	}

}
