package chapter2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Question6 {
	public boolean palindrome(LinkedListNode list1, LinkedListNode list2){
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		LinkedListNode.Node p1 = list1.head;
		while(p1 != null){
			if(map.containsKey(p1.data)){
				map.put(p1.data, (map.get(p1.data))+1);
				
			}else{
				map.put(p1.data, 1);
			}
			p1 = p1.next;
		}
		
		LinkedListNode.Node p2 = list2.head;
		while(p2 != null){
			if(map.containsKey(p2.data)){
				map.put(p2.data, (map.get(p2.data))-1);
				
			}else{
				return false;
			}
			p2 = p2.next;
		}
		
		Iterator<Integer> itr = map.keySet().iterator();
		while(itr.hasNext()){
			Integer key = itr.next();
			if(map.get(key) != 0)
				return false;
		}
		return true;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedListNode list1 = new LinkedListNode();
		LinkedListNode list2 = new LinkedListNode();
		
		list1.appendToTail(7);
		list1.appendToTail(1);
		list1.appendToTail(6);
		
		list2.appendToTail(6);
		list2.appendToTail(1);
		list2.appendToTail(7);
//		list2.appendToTail(7);
//		list2.appendToTail(2);
		
		Question6 test = new Question6();
		System.out.println(test.palindrome(list1, list2));
	}

}
