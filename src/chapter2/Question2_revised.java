package chapter2;

public class Question2_revised {
	void returnKthToLast(CJBLinkedList ll, int index) {
		CJBLinkedList.Node n = ll.head;
		int size = ll.size();
		for(int i = 0; i < size - index; i++) {
			n = n.next;
		}
		ll.head = n;
	}
	public static void main(String[] args) {
		Question2_revised obj = new Question2_revised();
	    CJBLinkedList test = new CJBLinkedList();
	    for(int i = 0; i < 10; i++) {
	    	test.appendToTail(i);
	    }
	    test.printList();
	    System.out.println(" ");
	    System.out.println("===");
	    obj.returnKthToLast(test, 2);
	    test.printList();
	}
}
