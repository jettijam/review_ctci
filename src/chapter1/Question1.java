package chapter1;

public class Question1 {
	
	boolean checkUniqueCharacter(String str)
	{
		for(int i = 0 ; i < str.length() ; i++) {
			for(int j = 0 ; j < i ; j ++) {
				if(str.charAt(i) == str.charAt(j)) {
					return false;
				}
			}
		}
		return true;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question1 prob1 = new Question1();
		System.out.println(prob1.checkUniqueCharacter("asdfasdf"));
		System.out.println(prob1.checkUniqueCharacter("helc"));
	}

}
