package chapter1;

public class Question3 {
	public String urlify(String str, int len){
		boolean startString = false;
		int startIdx = 0;
		int arrayLen = 0;
		for(int i = 0 ; i < len ; i++){
			if(!startString){
				if(str.charAt(i)!=' '){
					startString = true;
					startIdx = i;
				}
			}
			
			if(i <= startIdx + len){
				if(str.charAt(i) == ' ')
					arrayLen += 3;
				else
					arrayLen++;
			}else{
				break;
			}			
		}
		char[] input = new char[arrayLen];
		int idx = 0;
		for(int i = 0 ; i < str.length() ; i++){
			if(str.charAt(i)!=' '){
				input[idx++] = str.charAt(i);
			}else{
				input[idx++] = '%';
				input[idx++] = '2';
				input[idx++] = '0';
			}
			if(idx >= arrayLen)
				break;
		}
		return String.copyValueOf(input);
	}
	public static void main(String[] args) {
		Question3 test = new Question3();
		System.out.println(test.urlify("Mr John Smith    ", 13));
	}

}
