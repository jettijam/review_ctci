package chapter1;

public class Question1_revised_CJB {
	boolean checkUniqueCharacter(String str)
	{
		int[] checkChar = new int[256];
		for (int i = 0 ; i < str.length(); i++) {
			int charInt = str.charAt(i);
			if (checkChar[charInt] == 1) {
				return false;
			}
			checkChar[charInt] = 1;
		}
		return true;
	}
	public static void main(String[] args) {
		Question1_revised_CJB obj = new Question1_revised_CJB();
		System.out.println(obj.checkUniqueCharacter("asdfasdf"));
		System.out.println(obj.checkUniqueCharacter("helcc"));
	}
}
