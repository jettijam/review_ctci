package chapter1;

public class Question8 {
	public int[][] zeroMatrix(int[][] matrix){
		int result[][] = new int[matrix.length][matrix[0].length];
		for(int row = 0 ; row < matrix.length ; row++){
			for(int col = 0 ; col < matrix[0].length ; col++){
				result[row][col] = matrix[row][col];
			}
		}
		
		for(int row = 0 ; row < matrix.length ; row++){
			for(int col = 0 ; col < matrix[0].length ; col++){
				if(matrix[row][col] == 0){
					for(int i = 0 ; i < matrix[0].length ; i++){
						result[row][i] = 0;
					}
					for(int i = 0  ; i < matrix.length ; i++){
						result[i][col] = 0;
					}
				}
			}
		}
		return result;
	}
	public static void main(String[] args) {
		int img[][] = new int[5][5];
		for(int i = 0 ; i < 5 ; i++){
			for(int j = 0 ; j < 5 ; j++){
				img[i][j] = i*5 + j;
			}
		}
		
		img[2][2] = 0;
		
		for(int i = 0 ; i < 5 ; i++){
			for(int j = 0 ; j < 5 ; j++){
				System.out.print(img[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println();
		Question8 test = new Question8();
		img = test.zeroMatrix(img);
		
		for(int i = 0 ; i < 5 ; i++){
			for(int j = 0 ; j < 5 ; j++){
				System.out.print(img[i][j]+" ");
			}
			System.out.println();
		}
	}

}
