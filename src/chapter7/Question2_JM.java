package chapter7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Question2_JM {
	
	Queue<Respondant> freeRespondant = new LinkedList<Respondant>();
	ArrayList<Respondant> respondants = new ArrayList<Respondant>();
	
	Queue<Manager> freeManager = new LinkedList<Manager>();
	ArrayList<Manager> managers = new ArrayList<Manager>();
	
	Queue<Director> freeDirector = new LinkedList<Director>();
	ArrayList<Director> directors = new ArrayList<Director>();
	
	public Question2_JM(){
		for(int i = 0 ; i < 10 ; i++){
			respondants.add(new Respondant());
			managers.add(new Manager());
			directors.add(new Director());
		}
		for(Respondant e : respondants){
			if(!e.isBusy){
				freeRespondant.add(e);
			}
		}
		for(Manager e : managers){
			if(!e.isBusy){
				freeManager.add(e);
			}
		}
		for(Director e : directors){
			if(!e.isBusy){
				freeDirector.add(e);
			}
		}
	}
	
	public boolean dispatchCall(Call incoming){
		if(!freeRespondant.isEmpty()){
			incoming.doer = freeRespondant.poll();
			return true;
		}
		if(!freeManager.isEmpty()){
			incoming.doer = freeManager.poll();
			return true;
		}
		if(!freeDirector.isEmpty()){
			incoming.doer = freeDirector.poll();
			return true;
		}
		return false;
	}
	
}

class Call{
	Employee doer;
}
class Employee{
	boolean isBusy = false;
}
class Respondant extends Employee{}
class Manager extends Employee{}
class Director extends Employee{}
