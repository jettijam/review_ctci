package chapter7;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Question10_JM {
	public static void main(String[] args){
		MineSweeper ms = new MineSweeper(5, 5, 2);
		
		ms.play();
	}
}
class MineSweeper{
	Board_JM board;
	public MineSweeper(int w, int h, int numMines){
		this.board = new Board_JM(w, h, numMines);
		this.board.printBoard(false);
	}
	
	public void play(){
		boolean result = true;
		Scanner input = new Scanner(System.in);
		int x = -1;
		int y = -1;
		while(result){
			System.out.println("x : ");
			x = Integer.valueOf(input.nextLine());
			System.out.println("y : ");
			y = Integer.valueOf(input.nextLine());
			
			result = click(x, y);
		}
		
	}
	
	public boolean click(int x, int y){
		if(board.click(x, y)){
			board.expand(x,  y);
			board.printBoard(false);
			if(board.isFull()){
				System.out.println("CONGRATULATION!");
				return false;
			}
			return true;
		}else{
			System.out.println("WOW YOU FUCKEDUP1");
			board.printBoard(true);
			return false;
		}
		
	}
	
	
}

class Board_JM{
	Tile board[][] = null;
	int w;
	int h;
	int numMine;
	ArrayList<Coord> mines = new ArrayList<Coord>();
	public Board_JM(int w, int h, int numMine){
		this.w = w;
		this.h = h;
		this.numMine = numMine;
		board = new Tile[h][w];
		
		init();
	}
	
	public void init(){
		
		for(int y = 0  ; y < h ; y++){
			for(int x = 0 ; x < w ; x++){
				board[y][x] = new Tile(x, y);
			}
		}
		
		Random r = new Random();
		for(int i = 0 ; i < numMine ; i++){
			int y = -1;
			int x = -1;
			do{
				y = r.nextInt(h);
				x = r.nextInt(w);
				boolean found = false;
				for(int idx = 0 ; idx < mines.size() ; idx++){
					Coord c = mines.get(idx);
					if( c.x == x && c.y == y){
						found = true;
						break;
					}
				}
				if(!found) break;
			}while(true);
			
			mines.add(new Coord(x, y));
			board[y][x] = new Mine(x, y);
		}
		
		for(int y = 0 ; y < h ; y++){
			for(int x = 0 ; x < w ; x++){
				int cnt = searchNearMine(x, y);
				if( cnt >= 0 ){
					board[y][x].setValue(cnt);
				}
			}
		}
		
	}
	
	public boolean click(int x, int y){ // if false you're doomed
		if(board[y][x].getClass() == Mine.class)
			return false;
		return true;
	}
	
	public boolean isFull(){
		for(int y = 0 ; y < h ; y ++){
			for(int x = 0 ; x < w ; x++){
				if(board[y][x].getClass() == Mine.class)
					continue;
				if(board[y][x].isHidden == true)
					return false;
			}
		}
		return true;
	}
	
	public void expand(int x, int y){
		if(board[y][x].getClass() == Mine.class) 
			return;
		if(!board[y][x].isHidden)
			return;
		
		board[y][x].isHidden = false;
		
		if(board[y][x].val == 0){
			if( x-1 >= 0 && y-1 >= 0){
				expand(x-1, y-1);
			}
			if( x-1 >= 0 ){
				expand(x-1, y);
			}
			if( x-1 >= 0 && y+1 < h){
				expand(x-1, y+1);
			}
			if( y-1 >= 0 ){
				expand(x, y-1);
			}
			if( y+1 < h){
				expand(x, y+1);
			}
			if( x+1 < w && y-1 >= 0){
				expand(x+1, y-1);
			}
			if( x+1 < w ){
				expand(x+1, y);
			}
			if( x+1 < w && y+1 < h){
				expand(x+1, y+1);
			}
			
		}
		
	}
	
	public int searchNearMine(int x, int y){ // return -1 when (x, y) stands for a mine
		int cnt = 0;
		if(board[y][x].getClass() == Mine.class)
			return -1;
		
		if( x-1 >= 0 && y-1 >= 0){
			if( board[y-1][x-1].getClass() == Mine.class ){
				cnt ++;
			}
		}
		
		if( x-1 >= 0 ){
			if( board[y][x-1].getClass() == Mine.class )
				cnt ++;
		}
		
		if( x-1 >= 0 && y+1 < h){
			if( board[y+1][x-1].getClass() == Mine.class )
				cnt++;
		}
		
		if( y-1 >= 0 ){
			if( board[y-1][x].getClass() == Mine.class )
				cnt++;
		}
		if(y+1 < h){
			if( board[y+1][x].getClass() == Mine.class )
				cnt++;
		}
		if(x+1 < w && y-1 >= 0){
			if( board[y-1][x+1].getClass() == Mine.class )
				cnt++;
		}
		if(x+1 < w ){
			if( board[y][x+1].getClass() == Mine.class )
				cnt++;
		}
		if(x+1 < w && y+1 < h){
			if( board[y+1][x+1].getClass() == Mine.class)
				cnt++;
		}
		return cnt;
	}
	
	public void printBoard(boolean showHidden){
		for(int y = 0 ; y < h ; y ++){
			for(int x = 0 ; x < w ; x++){
				if(showHidden){
					if(board[y][x].getClass() == Mine.class){
						System.out.print(" *");
					}else{
						System.out.print( String.format("%2d", board[y][x].val) ) ;
					}
				}else{
					if(board[y][x].isHidden)
						System.out.print(" -");
					else{
						if(board[y][x].getClass() == Mine.class){
							System.out.print(" *");
						}else{
							System.out.print( String.format("%2d", board[y][x].val) ) ;
						}
					}
				}
			}
			System.out.println();
			
		}
		System.out.println();
	}
}
class Tile{
	int x;
	int y;
	int val=0;
	boolean isHidden = true;
	public Tile(int x, int y){
		this.y = y;
		this.x = x;
	}
	
	public void setValue(int val){
		this.val = val;
	}
}

class Mine extends Tile{
	
	public Mine(int x, int y) {
		
		super(x, y);
		this.val = 99;
	}
	
}

class Coord{
	int x;
	int y;
	public Coord(int x, int y){
		this.x = x;
		this.y = y;
	}
}