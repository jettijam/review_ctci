package chapter7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Question6_JB {
	class PuzzlePiece {
		private Edge leftEdge = null;
		private Edge rightEdge = null;
		private Edge topEdge = null;
		private Edge bottomEdge = null;
		public void setLeftEdge(Edge e) {
			this.leftEdge = e;
		}
		public void setRightEdge(Edge e) {
			this.rightEdge = e;
		}
		public void setTopEdge(Edge e) {
			this.topEdge = e;
		}
		public void setBottomEdge(Edge e) {
			this.bottomEdge = e;
		}
		public Edge getLeftEdge() {
			return this.leftEdge;
		}
		public Edge getRightEdge() {
			return this.rightEdge;
		}
		public Edge getTopEdge() {
			return this.topEdge;
		}
		public Edge getBottomEdge() {
			return this.bottomEdge;
		}
	}
	class Edge {
		
	}
	class Puzzle {
		private PuzzlePiece[][] pieces;
		private PuzzlePiece[][] solution;
		public Puzzle(int n) {
			this.pieces = new PuzzlePiece[n][n];
			for(int i = 0; i < n; i ++) {
				for(int j = 0; j < n-1; j++) {
					Edge e1 = new Edge();
					pieces[i][j].setRightEdge(e1);
					pieces[i][j+1].setLeftEdge(e1);
					Edge e2 = new Edge();
					pieces[j][i].setBottomEdge(e2);
					pieces[j+1][i].setTopEdge(e2);
				}
			}
			shuffle(pieces);
		}
		
		public PuzzlePiece[][] findSolution() {
			ArrayList<PuzzlePiece> list = new ArrayList<PuzzlePiece>();
			for (PuzzlePiece[] array : this.pieces) {
				list.addAll(Arrays.asList(array));
			}
			for(int i = 0; i < list.size(); i++) {
				if (list.get(i).getLeftEdge() == null && list.get(i).getTopEdge() == null) {
					this.solution[0][0] = list.get(i);
				}
			}
			for(int j = 0; j < this.solution[0].length - 1; j++) {
				int index = 0;
				for(int i = 0; i < list.size(); i++) {
					if(fitsWith(this.solution[j][index].getRightEdge(), list.get(i).getLeftEdge())) {
						this.solution[j][++index] = list.get(i); 
					}
					if(fitsWith(this.solution[j][0].getBottomEdge(), list.get(i).getTopEdge())) {
						this.solution[j+1][0] = list.get(i);
					}
				}
			}
			return this.solution;
		}
	}
	
	void shuffle(PuzzlePiece[][] p) {
	    Random random = new Random();

	    for (int i = p.length - 1; i > 0; i--) {
	        for (int j = p[i].length - 1; j > 0; j--) {
	            int m = random.nextInt(i + 1);
	            int n = random.nextInt(j + 1);

	            PuzzlePiece temp = p[i][j];
	            p[i][j] = p[m][n];
	            p[m][n] = temp;
	        }
	    }
	}
	
	public boolean fitsWith(Edge e1, Edge e2) {
		if(e1 == e2) {
			return true;
		}
		return false;
	}
}
