package chapter7;

import java.sql.Timestamp;
import java.util.ArrayList;

public class Question11_JM {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

class MetaData{
	Timestamp created = null;
	Timestamp modified = null;
	
}

class File{
	Directory parent;
	String name;
	byte[] content;
	MetaData metaData;
	
	public boolean isDirectory(){return false;}
}
class Directory extends File{
	ArrayList<File> children = new ArrayList<File>();
	
	public void printAllChildren(){
		for(File c : children){
			if(c.isDirectory()){
				((Directory)c).printAllChildren();
			}
		}
	}
	
	@Override
	public boolean isDirectory(){return true;}
}
