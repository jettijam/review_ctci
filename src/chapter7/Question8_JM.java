package chapter7;

public class Question8_JM {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Field test = new Field();
		test.printField();
		
//		if(test.moveable(6,  4,  Face.BLACK))
		test.move(6, 4, Face.BLACK);
		test.moveable(8,  4,  Face.BLACK);
		test.printField();
//		if(test.moveable(2,  1,  Face.WHITE))
//			test.move(2, 1, Face.WHITE);
//		if(test.moveable(4,  1,  Face.BLACK))
//			test.move(4, 1, Face.BLACK);
	}

}

class Game{
	//TODO
}

class Player{
	//TODO
}
class Field{
	private int size = 10;
	private OthelloPiece field[][] = new OthelloPiece[size][size];
	public Field(){
		field[4][4] = new OthelloPiece(Face.BLACK);
		field[5][4] = new OthelloPiece(Face.WHITE);
		field[4][5] = new OthelloPiece(Face.WHITE);
		field[5][5] = new OthelloPiece(Face.BLACK);
	}
	public void printField(){
		for(int y = 0 ; y < size ; y++){
			for(int x = 0 ; x < size ; x++){
				if(field[y][x] == null)
					System.out.print("- ");
				else
					field[y][x].printPiece();
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public void move(int x, int y, Face face){
//		if(moveable(x, y, face)){
		field[y][x] = new OthelloPiece(face);
//		}
		refresh(x, y, face);
		printField();
	}
	
	public boolean moveable(int x, int y, Face face){
		if(field[y][x] != null){
			System.out.println("("+x+","+y+") is already filled");
			return false;
		}
		if(!flippable(x, y, face)){
			System.out.println("("+x+","+y+") is not flippable");
			return false;
		}
		return true;
	}
	public boolean flippable(int x, int y, Face face){
		
		OthelloPiece tmpField[][] = new OthelloPiece[size][size];
		for(int i = 0 ; i < size ; i++){
			for(int j = 0 ; j < size ; j++){
				if(field[i][j] == null)
					continue;
				if(field[i][j].getFace() == Face.BLACK){
					tmpField[i][j] = new OthelloPiece(Face.BLACK);
				}
				if(field[i][j].getFace() == Face.WHITE){
					tmpField[i][j] = new OthelloPiece(Face.WHITE);
				}
			}
		}
		
		field[y][x] = new OthelloPiece(face);
		if(!refresh(x, y, face)){
			field = tmpField;
			System.out.println("("+x+","+y+") refresh returned false");
			return false;
		}
		
		for(int j = 0 ; j < size ; j++){
			for(int i = 0 ; i < size ; i++){
				if(i == x && j == y){
					continue;
				}
//				if( !( field[j][i] == null && tmpField[j][i] == null) ){
//					field = tmpField;
//					System.out.println("false 1");
//					return false;
//				}
				if( field[j][i] != null ){
					if(tmpField[j][i] == null){
						field = tmpField;
						System.out.println("false 2");
						return false;
					}
					if(field[j][i].getFace() != tmpField[j][i].getFace()){
						field = tmpField;
						System.out.println("false 3");
						return false;
					}
				}else{
					if(tmpField[j][i] != null){
						field = tmpField;
						System.out.println("false 4");
						return false;
					}
				}
			}
		}
		
		return true;
	}
	public boolean refresh(int x, int y, Face face){
		boolean result = false;
		int curX = x;
		int curY = y;
		while(++curX < size){
			if(field[y][curX] == null)
				break;
			if( field[y][curX].getFace() == face ){
				for(int i = x ; i < curX ; i++){
					field[y][i].setFace(face);
				}
				
				result = true;
				break;
			}  
		}
		curX = x;
		while(--curX >= 0){
			if(field[y][curX] == null)
				break;

			if( field[y][curX].getFace() == face ){
				for(int i = curX ; i < x ; i++){
					field[y][i].setFace(face);
				}
				result = true;
				break;
			}
		}
		curX = x;
		while(++curY < size){
			if(field[curY][x] == null)
				break;

			if( field[curY][x].getFace() == face ){
				for(int i = y ; i < curY ; i++){
					field[i][x].setFace(face);
				}
				result = true;
				break;
			}
		}
		curY = y;
		while(--curY >= 0){
			if(field[curY][x] == null)
				break;

			if( field[curY][x].getFace() == face ){
				for(int i = curY ; i < y ; i++){
					field[i][x].setFace(face);
				}
				result = true;
				break;
			}
		}
		
		return result;
	}
}
class OthelloPiece{
	private Face face;
	public OthelloPiece(Face f){
		this.face = f;
	}
	public Face getFace(){
		return face;
	}
	public void setFace(Face face){
		this.face = face;
	}
	public void printPiece(){
		if(face == Face.BLACK)
			System.out.print("X ");
		else
			System.out.print("O ");
	}
}
enum Face{BLACK, WHITE}