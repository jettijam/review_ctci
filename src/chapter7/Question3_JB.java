package chapter7;

import java.util.ArrayList;

public class Question3_JB {
	public enum Genre {
		ROCK, HIPHOP, BALLAD, POP, UNKNOWN
	}
	class MusicalJukebox {
		private ArrayList<Song> playList;
		private ArrayList<Song> songList;
		
		public void appendToPlayList(Song song) {
			this.playList.add(song);
		}
		
		public void appendToSongList(Song song) {
			this.playList.add(song);
		}
		
		public ArrayList<Song> getPlayList() {
			return this.playList;
		}
		
		public ArrayList<Song> getSongList() {
			return this.songList;
		}
		
		public ArrayList<Song> findSpecificGenre(Genre type) {
			ArrayList<Song> list = new ArrayList<Song>();
			for(int i = 0 ; i < this.songList.size(); i++) {
				Song song = this.songList.get(i);
				if(song.getGenre() == type) {
					list.add(song);
				}
			}
			return list;
		}
		
		public boolean checkSongInTheListByTitle(String title) {
			for(int i = 0; i < this.songList.size(); i++) {
				Song song = this.songList.get(i);
				if(song.getTitle() == title) {
					return true;
				}
			}
			return false;
		}
	}
	class Song {
		private Genre type;
		private String title; 
		
		public Song(Genre type, String title) {
			this.type = type;
			this.title = title;
		}
		
		public String getTitle() {
			return this.title;
		}
		
		public Genre getGenre() {
			return this.type;
		}
	}
}
