package chapter7;

import java.util.ArrayList;
import java.util.Iterator;

public class Question9_JB {
	public class CircularArray<T> implements Iterable<T>{
		private ArrayList<T> items;
		private int head;
		
		public CircularArray(int size) {
			this.items = new ArrayList<T>(size);
			this.head = 0;
		}
		
		public void rotate(int rot) {
			int prevHead = this.head;
			int newHead = (prevHead + rot) % (this.items.size());
			this.head = newHead;
		}
		
		public void appendItem(T item) {
			this.items.add(item);
		}
		
		public T getItem(int index) {
			return this.items.get(index);
		}
		
		public T getHead() {
			return this.items.get(this.head);
		}

		private class CircularArrayIterator<TI> implements Iterator<TI> {
			public int _current = -1;
			private ArrayList<TI> _items;
			
			public CircularArrayIterator(CircularArray<TI> array) {
				this._items = array.items;
			}
			
			@Override
			public boolean hasNext() {
				return _current < items.size() - 1;
			}
			@Override
			public TI next() {
				_current++;
				TI item = (TI) _items.get(_current);
				return item;
			}
			@Override
			public void remove() {
				throw new UnsupportedOperationException("...");
			}
		}
		
		public Iterator<T> iterator() {
			return new CircularArrayIterator<T>(this);
		}
	}
	
	public static void main(String[] args) {
		Question9_JB obj = new Question9_JB();
		CircularArray<Integer> test = obj.new CircularArray<Integer>(10);
		for(int i = 0 ; i < 10; i ++) {
			test.appendItem(i);
		}
		for(Integer x : test) {
			System.out.println(x);
		}
	}
}
