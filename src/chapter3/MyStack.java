package chapter3;


public class MyStack {
	private class StackNode{
		private int data;
		private StackNode next;
		public StackNode(int d){
			data = d;
		}
	}
	private StackNode top;
	
	
	public int pop() throws EmptyStackException{
		if(top == null)
			throw new EmptyStackException();
		
		int item = top.data;			
		top = top.next;
		
		return item;
	}
	
	public void push(int item){
		StackNode node = new StackNode(item);
		
		node.next = top;
		top = node;
	}
	
	public int peek() throws EmptyStackException{
		if(top == null)
			throw new EmptyStackException();
		return top.data;
	}
	
	public boolean isEmpty(){
		return top == null;
	}
	
	
	public void printStack(){
		StackNode cursor = top;
		while(cursor != null){
			System.out.println(cursor.data);
			cursor = cursor.next;
		}
	}
	
}

class EmptyStackException extends Exception{
	
}