package chapter3;

public class Question2 {
	class MinStack{
		private class StackNode{
			private int data;
			private StackNode next;
			public StackNode(int d){
				data = d;
			}
		}
		private StackNode top;
		private StackNode minNode = null;
		private int minVal = Integer.MAX_VALUE;
		
		
		public int pop() throws EmptyStackException{
			if(top == null)
				throw new EmptyStackException();
			
			boolean minFlag = minNode == top;
			
			int item = top.data;			
			top = top.next;
			
			if(minFlag){
				minVal = Integer.MAX_VALUE;
				StackNode cursor = top;
				while(cursor != null){
					if(cursor.data < minVal){
						minVal = cursor.data;
						minNode = cursor;
					}
					cursor = cursor.next;
				}
			}
			
			return item;
		}
		
		public void push(int item){
			StackNode node = new StackNode(item);
			if(item < minVal){
				minVal = item;
				minNode = node;
			}
			
			node.next = top;
			top = node;
		}
		
		public int peek() throws EmptyStackException{
			if(top == null)
				throw new EmptyStackException();
			return top.data;
		}
		
		public boolean isEmpty(){
			return top == null;
		}
		
		public int min() throws EmptyStackException{
			return minVal;
		}
		
		public void printStack(){
			StackNode cursor = top;
			while(cursor != null){
				System.out.println(cursor.data);
				cursor = cursor.next;
			}
		}
	}
	class EmptyStackException extends Exception{
		
	}
	
	
	public static void main(String[] args){
		Question2 test = new Question2();
		MinStack stack = test.new MinStack();
		stack.push(1);
		stack.push(2);
		stack.push(0);
		stack.push(-1);
		stack.push(-2);
		
		
		stack.printStack();
		
		System.out.println("****************");
		try {
			System.out.println(stack.min());
			stack.pop();
			System.out.println(stack.min());
		} catch (EmptyStackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
