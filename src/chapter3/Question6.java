package chapter3;

import java.util.LinkedList;

public class Question6 {
	class Animal{
	}
	class Dog extends Animal{
	}
	class Cat extends Animal{
	}
	class AnimalShelter{
		LinkedList<Boolean> isCat = new LinkedList<Boolean>();
		LinkedList<Dog> dogQ = new LinkedList<Dog>();
		LinkedList<Cat> catQ = new LinkedList<Cat>();
		
		public void enqueue(Animal item){
			if(item.getClass() == Dog.class){
				dogQ.add((Dog)item);
				isCat.add(false);
			}else{
				catQ.add((Cat)item);
				isCat.add(true);
			}
		}
		
		public Animal dequeueAny(){
			Animal result = null;
			if(isCat.removeFirst()){
				result = catQ.removeFirst();
			}else{
				result = dogQ.removeFirst();
			}
			return result;
		}
		
		public Animal dequeueCat(){
			Animal result = catQ.removeFirst();
			for(int i = 0 ; i < isCat.size(); i++){
				if(isCat.get(i)){
					isCat.remove(i);
					break;
				}
			}
			return result;
		}
		
		public Animal dequeueDog(){
			Animal result = dogQ.removeFirst();
			for(int i = 0 ; i < isCat.size(); i++){
				if(!isCat.get(i)){
					isCat.remove(i);
					break;
				}
			}
			return result;
		}
		
		public void printShelter(){
			System.out.println("==== CAT ====");
			for(int i = 0 ; i < catQ.size() ; i++){
				System.out.println("Cat #"+(i+1));
			}
			System.out.println("==== DOG ====");
			for(int i = 0 ; i < dogQ.size() ; i++){
				System.out.println("Dog #"+(i+1));
			}
			System.out.println();
		}
	}
	class EmptyQueueException extends Exception{}
	
	public static void main(String[] args){
		Question6 test = new Question6();
		AnimalShelter shelter = test.new AnimalShelter();
		shelter.enqueue(test.new Cat());
		shelter.enqueue(test.new Cat());
		shelter.enqueue(test.new Dog());
		shelter.enqueue(test.new Cat());
		shelter.enqueue(test.new Cat());
		shelter.enqueue(test.new Dog());
		shelter.printShelter();
		
		shelter.dequeueAny();
		shelter.printShelter();
		shelter.dequeueAny();
		shelter.printShelter();
		shelter.dequeueCat();
		shelter.printShelter();
		shelter.dequeueAny();
		shelter.printShelter();
		
//		MyQueue q = test.new MyQueue();
//		q.enqueue(5);
//		q.enqueue(5);
//		q.enqueue(5);
//		
//		try {
//			System.out.println(q.dequeue());
//			System.out.println(q.dequeue());
//			System.out.println(q.dequeue());
//			System.out.println(q.dequeue());
//		} catch (EmptyQueueException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		
	}
}
