package chapter3;

import java.util.EmptyStackException;

public class Question3 {
	public class setOfStacks<T> {
		private class singleStack<T> {
			private class StackNode<T> {
				private T data;
				private StackNode<T> next;
				
				public StackNode(T data) {
					this.data = data;
				}
			}
			
			private StackNode<T> top;
			private int size;
			private int max;
			
			public T pop() {
				if(top == null) throw new EmptyStackException();
				T item = top.data;
				top = top.next;
				this.size--;
				return item;
			}
			
			public void push(T item) {
				StackNode<T> t = new StackNode<T>(item);
				t.next = top;
				top = t;
				this.size++;
			}
			
			public T peek() {
				if (top == null) throw new EmptyStackException();
				return top.data;
			}
			
			public boolean isEmpty() {
				return top == null;
			}
			
			public boolean isFull() {
				return this.size == this.max;
			}
			
			public void setMax(int max) {
				this.max = max;
			}
			
			private singleStack<T> prevStack;
		}
		
		private singleStack<T> lastStack;
		private int max;
		
		public void push(T item) {
			if(lastStack == null) {
				singleStack<T> s = new singleStack<T>();
				s.max = this.max;
				this.lastStack = s;
			}
			else if(lastStack.isFull()) {
				singleStack<T> s = new singleStack<T>();
				s.max = this.max;
				s.prevStack = lastStack;
				this.lastStack = s;
			}
			this.lastStack.push(item);
		}
		
		public T pop() {
			if(lastStack == null) throw new EmptyStackException();
			T item = lastStack.pop();
			if(lastStack.isEmpty()) {
				this.lastStack = lastStack.prevStack;
			}
			return item;
		}
		
		public void setMax(int max) {
			this.max = max;
		}
	}
	public static void main(String[] args) {
		Question3 obj = new Question3();
		setOfStacks<Integer> test = obj.new setOfStacks<Integer>();
		test.setMax(3);
		test.push(1);
		test.push(2);
		test.push(3);
		test.push(4);
		System.out.println(test.pop());
		System.out.println(test.pop());
		System.out.println(test.pop());
	}
}
