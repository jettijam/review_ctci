package chapter3;

public class Question4 {
	public class MyQueue<T> {
		CJBStack<T> stack1 = new CJBStack<T>();
		CJBStack<T> stack2 = new CJBStack<T>();
		
		public void add(T item) {
			if(stack1.isEmpty()) {
				while(stack2.isEmpty() == false) {
					stack1.push(stack2.pop());
				}
			}
			stack1.push(item);
		}
		
		public T remove() {
			if(stack2.isEmpty()) {
				while(stack1.isEmpty() == false) {
					stack2.push(stack1.pop());
				}
			}
			return stack2.pop();
		}
		
		public T peek() {
			if(stack2.isEmpty()) {
				while(stack1.isEmpty() == false) {
					stack2.push(stack1.pop());
				}
			}
			return stack2.peek();
		}
		
		public boolean isEmpty() {
			return (stack1.isEmpty() && stack2.isEmpty());
		}
	}
	public static void main(String[] args) {
		Question4 obj = new Question4();
		MyQueue<Integer> test = obj.new MyQueue<Integer>();
		test.add(1);
		test.add(2);
		test.add(3);
		System.out.println(test.peek());
		System.out.println(test.remove());
		System.out.println(test.peek());
	}
}
