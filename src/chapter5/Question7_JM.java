package chapter5;

public class Question7_JM {
	public int pairwiseSwap(int num){
		System.out.println("input num : "+Integer.toBinaryString(num));
		int evenBits = num & 0xaaaaaaaa;
		System.out.println("even bits  : "+Integer.toBinaryString(evenBits));
		int oddBits = num & 0x55555555;
		System.out.println("odd bits  : "+Integer.toBinaryString(oddBits));
		
		
		return (evenBits>>>1)|(oddBits<<1);
	}
	
	public static void main(String[] args){
		int num = 10;
		Question7_JM test = new Question7_JM();
		System.out.println(Integer.toBinaryString(test.pairwiseSwap(num)));
	}
}
