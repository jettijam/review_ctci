package chapter5;

public class Question4_JM {
	public void nextNumber(int num){
		System.out.println("Input num :"+Integer.toBinaryString(num));
		System.out.println("next smallest :"+Integer.toBinaryString(nextSmallest2(num)));
		System.out.println("prev largest  :"+Integer.toBinaryString(prevLargest2(num)));
	}
	
	public int nextSmallest(int num){
		int oneCnt = oneCount(num);
		if(oneCnt == 1){
			return num << 1;
		}
		int len = (int)(Math.log(num)/Math.log(2)) + 1;
		int one = 1;
		for(int i = 0 ; i < (len - Integer.toBinaryString(num).lastIndexOf("1")) ; i++){
			one <<= 1;
			one |= 1;
		}
//		int one = 1 << (len - Integer.toBinaryString(num).lastIndexOf("1"));
		System.out.println(Integer.toBinaryString(one));
		
		return num ^ one;
	}
	
	public int nextSmallest2(int num){
		int copy = num;
		int zeroIdx = -1;
		int oneIdx = -1;
		for(int i = 0 ; i < 32 ; i++){
			if(oneIdx < 0 && (copy & 1) == 1){
				oneIdx = i;
			}
			if(zeroIdx < oneIdx && (copy & 1) == 0){
				zeroIdx = i;
			}
			if(zeroIdx > oneIdx && oneIdx > -1){
				break;
			}
			copy >>>= 1;
		}
		
		num >>>=(oneIdx+1);
//		System.out.println(Integer.toBinaryString(num));
		num <<= (oneIdx+1);
		
		int mask = 1;
		mask <<= zeroIdx;
		num |= mask;
		mask = 0;
		
		
	
		return num;
	}
	
	public int prevLargest(int num){
		int zeroCnt = zeroCount(num);
		
		if(zeroCnt == 0 ){
			return num << 1;
		}
		
		int len = (int)(Math.log(num)/Math.log(2)) + 1;
		int ones = 1;
		for(int i = 0 ; i < len-1 ; i++){
			ones <<= 1;
			ones |= 1;
		}
//		System.out.println(Integer.toBinaryString(ones));
		
		int zeros = (~0) << zeroCnt;
		int result = zeros & ones;
		if(result == num){
			return num << 1;
		}
		return result;
	}
	
	public int prevLargest2(int num){
		int copy = num;
		int zeroIdx = -1;
		int oneIdx = -1;
//		System.out.println(Integer.toBinaryString((copy&3)^2));
		for(int i = 0 ; i < 32 ; i++){
//			System.out.println(Integer.toBinaryString((copy&3)) + "vs. "+Integer.toBinaryString((2)));
			if((copy&3)==2){
				oneIdx = i+1;
				zeroIdx = i;
				break;
			}
			copy >>>= 1;
		}
//		System.out.println("zero :"+zeroIdx+" one : "+oneIdx);
		
		int mask = 1;
		mask <<= zeroIdx;
		num |= mask;
		
		mask = 1;
		mask <<= oneIdx;
		num ^= mask;
		
	
		return num;
	}

	private int oneCount(int num){
		String bit = Integer.toBinaryString(num);

		int cnt = 0;
		for(int idx = 0 ; idx < bit.length() ; idx++){
			if(bit.charAt(idx) == '1')
				cnt++;
		}
		return cnt;
	}
	private int zeroCount(int num){
		String bit = Integer.toBinaryString(num);

		int cnt = 0;
		for(int idx = 0 ; idx < bit.length() ; idx++){
			if(bit.charAt(idx) == '0')
				cnt++;
		}
		return cnt;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question4_JM test = new Question4_JM();
		test.nextNumber(9);
//		test.nextSmallest2(8);
	}

}
