package chapter5;

public class Question2 {
	char[] binaryToString(double num) {
		// 2를 곱하고 1을 넘으면 1을, 1을 넘지 않으면 0 을 charArray 에 append
		// 2를 곱해서 정확히 1이 되거나 charArray size 를 넘어가면 끝
		char[] charArray = new char[32];
		double n = num;
		double temp;
		int i = 0;
		while(true) {
			if(i > 31) {
				System.out.println("ERROR");
				break;
			}
			temp = n * 2;
			if(temp == 1) {
				charArray[i] = '1';
				break;
			}
			else if(temp < 1) {
				charArray[i] = '0';
				i++;
				n = temp;
			}
			else {
				charArray[i] = '1';
				i++;
				n = temp - 1;
			}
		}
		return charArray;
	}
	public static void main(String[] args) {
		Question2 obj = new Question2();
		double test = 0.6875;
		char[] array = obj.binaryToString(test);
		System.out.print("0.");
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i]);			
		}
	}
}
